const Sequelize = require('sequelize')
const sequelize = new Sequelize(
    'Email',
    'postgres',
    '1111',
    {
        dialect: 'postgres',
    }
)

class User extends Sequelize.Model { };
class Folder extends Sequelize.Model { };
class Message extends Sequelize.Model { };
class Folder_Message extends Sequelize.Model { };

User.init({
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
    },
    login: {
        type: Sequelize.TEXT,
        allowNull: false,
        comment: "User's login",
    },
    password: {
        type: Sequelize.TEXT,
        allowNull: false,
        comment: "User's password",
    },
}, { sequelize, modelName: 'user', tableName: 'users', timestamps: false })
Folder.init({
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
    },
    name: {
        type: Sequelize.TEXT,
        allowNull: false,
        comment: "Folder's name",
    },
    amount: {
        type: Sequelize.INTEGER,
        allowNull: false,
        comment: "Folder's amount",
    },
}, { sequelize, modelName: 'folder', tableName: 'folders', timestamps: false })
Message.init({
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
    },
    date: {
        type: Sequelize.DATE,
        allowNull: false,
        comment: "Message's date",
    },
    status: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        comment: "Folder's amount",
    },
    content: {
        type: Sequelize.TEXT,
        allowNull: false,
        comment: "Folder's name",
    },
}, { sequelize, modelName: 'message', tableName: 'messages', timestamps: false })
Folder_Message.init({}, { sequelize, modelName: 'folder_message', tableName: 'folders_messages', timestamps: false })

User.hasMany(Message, {
    foreignKey: 'user_id',
    allowNull: false,
    sourceKey: 'id',
});
Folder.belongsToMany(Message, {
    through: Folder_Message,
    foreignKey: 'folder_id',
})
Message.belongsToMany(Folder, {
    through: Folder_Message,
    foreignKey: 'message_id',
})

const { Client } = require('pg');

const Email = new Client({
    host: "localhost",
    port: 5432,
    user: "postgres",
    database: "Email",
    password: "1111"
})

Email.connect();

const Model = {

    async getFields(model) {
        let result = {};
        await model.findAll().then(res => { result = res[0].dataValues });
        delete result.id;
        return result;
    },

    async getLastId(entity) {
        let id;
        await Email.query(`SELECT "id" FROM ${entity}`).then(data => {
            id = data.rows[data.rows.length - 1].id;
        })
        return id;
    },

    async Insert(Entity, values) {
        let model;
        switch (Entity) {
            case "users":
                model = User;
                break;
            case "folders":
                model = Folder;
                break;
            case "messages":
                model = Message;
                break;
        }
        const Obj = await this.getFields(model);
        let i = 0;
        for (key in Obj) {
            Obj[key] = values[i];
            i++;
        }
        await model.create(Obj).catch(err => { throw new Error(err) });
    },

    async InsertRandom(Entity, amount, spec_mode) {
        let curr_id;
        await this.getLastId(Entity).then(data => { curr_id = data });
        let fields = [];
        await this.getFields(Entity).then(res => { fields = res });
        let types = [];
        await this.getTypes(Entity).then(res => { types = res });
        let values = [];
        console.log(fields, types);
        types.forEach((element, id) => {
            if (element === 25) {
                values[id] = "chr(trunc(65+random()*25)::int) || chr(trunc(65+random()*25)::int ) || chr(trunc(65+random()*25)::int ) || chr(trunc(65+random()*25)::int) || chr(trunc(65+random()*25)::int) || chr(trunc(65+random()*25)::int)";
            }
            if (element === 23) {
                values[id] = "trunc(random()*1000)::int";
            }
            if (element === 1082 || element === 1184) {
                values[id] = "timestamp '2014-01-10 20:00:00' +random() * (timestamp '2015-01-10 20:00:00' -timestamp '2014-01-10 10:00:00')";
            }
            if (element === 16) {
                values[id] = "(round(random())::int)::boolean";
            }
        });
        if (spec_mode != undefined) {
            const last = await this.getLastId("users");
            values[3] = `trunc(random()*${curr_id})::int`;
        }
        await Email.query(`INSERT INTO ${Entity} (${fields}) SELECT ${values} FROM generate_series(${curr_id + 1},${amount}+${curr_id})`)
    },

    async Delete(Entity, field, value) {
        let model;
        switch (Entity) {
            case "users":
                model = User;
                break;
            case "folders":
                model = Folder;
                break;
            case "messages":
                model = Message;
                break;
        }
        if (field === "id") {
            await model
                .destroy({
                    where: { id: value },
                })
               
        }
        if (field === "message_id") {
            await model
                .destroy({
                    where: { message_id: value },
                })
                
        }
        if (field === "user_id") {
            await model
                .destroy({
                    where: { user_id: value },
                })
                
        }
    },


    async Update(Entity, values, id) {
        let model;
        switch (Entity) {
            case "users":
                model = User;
                break;
            case "folders":
                model = Folder;
                break;
            case "messages":
                model = Message;
                break;
        }
        const Obj = await this.getFields(model);
        let i = 0;
        for (key in Obj) {
            Obj[key] = values[i];
            i++;
        }
        await model
        .update(
          Obj,
          {
            where: { id: id },
          }
        ).catch(err => { throw new Error(err) });
    },

    async GetEntity(Entity) {
        let result;
        const query = `SELECT * FROM ${Entity}`;
        await Email.query(query).catch(err => { throw new Error(err) }).then(res => { result = res.rows });
        return result;
    },

    async GetItemID(Entity, field, value) {
        let result = [];
        const query = `SELECT * FROM ${Entity} WHERE ${field}=${value}`;
        await Email.query(query).catch(err => { throw new Error(err) }).then(res => {
            res.rows.forEach(element => {
                result.push(element.id)
            })
        });
        return result;
    },
    async Join(Entities, fields, condition) {
        let result;
        const query = `SELECT ${fields} FROM ${Entities} WHERE ${condition}`;
        await Email.query(query).catch(err => { throw new Error(err) }).then(res => { result = res.rows });
        return result;

    },
};

module.exports = Model;

Model.Delete("users", "id", 1)   