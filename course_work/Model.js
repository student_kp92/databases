const readlineSync = require('readline-sync');
const moment = require('moment');
const axios = require('axios');
const fs = require("fs");
const Sequelize = require('sequelize')
var cmd = require('node-cmd');
let BD = new Sequelize(
    'Traffic',
    'postgres',
    '1111', {
    host: "127.0.0.1",
    port: 5434,
    dialect: 'postgres',
    timestamps: false,
},
)

const Model = {
    restore_master() {
        cmd.runSync(`systemctl stop postgresql@12-slave.service`)
        cmd.runSync(`sudo -u postgres rm -r /var/lib/postgresql/12/slave/`)
        cmd.runSync(`systemctl restart postgresql@12-main.service`)
        cmd.runSync(`sudo -u postgres pg_basebackup -h localhost -p 5432 -U standby -D /var/lib/postgresql/12/slave/ -Fp -Xs -R`)
        cmd.runSync(`systemctl restart postgresql@12-slave.service`)
        port=5432;
        BD = new Sequelize(
            'Traffic',
            'postgres',
            '1111', {
            host: "127.0.0.1",
            port: 5432,
            dialect: 'postgres',
            timestamps: false,
        },
        )
    },
    make_db_backup(name) {

        cmd.runSync(`pg_dump -U postgres --host "127.0.0.1" --port "5434" "Traffic" > ~/databases/course_work/backups/${name}.sql`);
        fs.appendFileSync("./backups/databases.txt", `\n${name} | ${Date()}`);
    },
    recovery_bd() {
        let arr = [];
        let available = [];
        let databases = fs.readFileSync("./backups/databases.txt", 'utf8');
        arr = databases.split('\n')
        let info = 'File name | date';
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].includes('|')) {
                arr[i] = arr[i].split(' | ');
                available.push(arr[i][0]);
                info += `\n${arr[i][0]} | ${arr[i][1]}`
            }
        }
        const answer = readlineSync.question(`Choose backup: 
${info}:
`);
        if (!available.includes(answer)) {
            throw new Error('Incorrect input. ');
        }
        //cmd.runSync(`systemctl stop postgresql@12-slave.service`);
        cmd.runSync(`psql -U postgres --host "127.0.0.1" --port "5434" -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'Traffic' AND pid <> pg_backend_pid()"`);
        cmd.runSync(`dropdb -U postgres --host "127.0.0.1" --port "5434" "Traffic" && createdb -U postgres --host "127.0.0.1" --port "5434" "Traffic" && psql -U postgres --host "127.0.0.1" --port "5434" "Traffic" < ~/databases/course_work/backups/${answer}.sql`);
        //cmd.runSync(`systemctl start postgresql@12-slave.service`);
    },
    make_table_backup(tablename) {
        fs.appendFileSync("./backups/tables.txt", `\n${tablename} | ${Date()}`);
        cmd.runSync(`pg_dump -U postgres --host "127.0.0.1" --port "5434" --file "/home/igor/databases/course_work/backups/${tablename}.sql" --no-password --verbose --format=c --blobs --table "public.${tablename}" "Traffic"`);
    },
    async recovery_table() {
        let arr = [];
        let available = [];
        let databases = fs.readFileSync("./backups/tables.txt", 'utf8');
        arr = databases.split('\n')
        let info = 'File name | date';
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].includes('|')) {
                arr[i] = arr[i].split(' | ');
                available.push(arr[i][0]);
                info += `\n${arr[i][0]} | ${arr[i][1]}`
            }
        }
        const answer = readlineSync.question(`Введите название файла резервной копии из списка: 
${info}: `);
        if (!available.includes(answer)) {
            throw new Error('Неверное название файла. ');
        }
        await BD.query(`TRUNCATE ${answer} CASCADE`);
        cmd.runSync(`pg_restore -U postgres --host "127.0.0.1" --port "5434" --no-password --dbname "Traffic" --data-only --verbose --schema "public" --table "${answer}" "/home/igor/databases/course_work/backups/${answer}.sql"`);
    },
    make_replication() {
        cmd.runSync("systemctl stop postgresql@12-main.service");
        cmd.runSync("systemctl stop postgresql@12-slave.service");
        cmd.runSync("sudo chmod 777 /var/lib/postgresql/12/slave/");
        cmd.runSync("sudo rm -rf /var/lib/postgresql/12/slave/standby.signal");
        cmd.runSync("sudo chmod 700 /var/lib/postgresql/12/slave/");
        cmd.runSync("systemctl start postgresql@12-slave.service");
        BD = new Sequelize(
            'Traffic',
            'postgres',
            '1111', {
            host: "127.0.0.1",
            port: 5434,
            dialect: 'postgres',
            timestamps: false,
        },
        )
        port=5434;
    },
    async DoQuery(query) {
        let result;
        await BD.query(query).then(res => { result = res[0] });
        return result;
    },
    
    async getIds(entity) {
        let ids = [];
        await BD.query(`SELECT "id" FROM ${entity} order by id`).then(data => 
            {
            
            data[0].forEach(elem => {
                ids.push(elem.id);
            })
        })
        return ids;
    },
    async Filter(Entity, condition) {
        let result;
        const query = `SELECT * FROM ${Entity} WHERE ${condition}`;
        await BD.query(query).catch(err => { throw new Error(err) }).then(res => { result = res.rows });
        return result;

    },
    async GenerateOwners(amount) {
        let str = "name,surname,pathronymic";
        const url = 'https://randomus.ru/name?type=0&sex=0&count=100';
        let res;
        await axios.get(url)
            .then(responce => { res = responce.data })
            .catch(error => { console.log(error) })
        res = res.slice(res.indexOf("<textarea"), res.indexOf("</textarea>"));
        res = res.slice(res.indexOf('">') + 2);
        res = res.split("\n");
        let names = [];
        let surnames = [];
        let papich = [];
        res.forEach((el) => {
            names.push(el.split(" ")[0]);
            surnames.push(el.split(" ")[1]);
            papich.push(el.split(" ")[2]);
        })
        let counter = 0;
        for (let i = 0; i < 100; i++) {
            for (let j = 0; j < 100; j++) {
                for (let k = 0; k < 100; k++) {
                    if (counter + 1 > amount) break;
                    str += `\n${surnames[i]},${names[j]},${papich[k]},${moment(this.generateRandomDate(new Date(1960, 0, 1), new Date(2000, 0, 1))).format('YYYY-MM-DD')}`
                    counter++;
                }

            }
        }
        fs.writeFileSync("./data/owners.csv", str);
        await BD.query(`copy owners("name", "surname", "pathronymic","birth_date") FROM '/home/igor/databases/course_work/data/owners.csv' WITH(FORMAT CSV,HEADER)`)
            .catch(err => { console.log(err) });
    },
    async GenerateCars(amount) {
        let raw = fs.readFileSync("./data/cars.json");
        data = JSON.parse(raw);
        let keys = Object.keys(data);
        let ids1 = await this.getIds("owners");
        let ids2 = await this.getIds("highways");
        let str = "brand,model,start_year,finish_year,engine_volume,owner_id,highway_id";
        let models = [];
        for (let i = 0; i < keys.length; i++) {
            for (let j = 0; j < data[keys[i]].length; j++) {
                models.push(`${keys[i]},${data[keys[i]][j].join(",")}`)

            }
        }
        for (let i = 0; i < amount; i++) {
            str += `\n${models[Math.floor(Math.random() * models.length)]},${Math.ceil(this.getRandomArbitrary(1, 9))},${ids1[Math.floor(Math.random() * ids1.length)]},${ids2[Math.floor(Math.random() * ids2.length)]}`;
        }
        fs.writeFileSync("./data/cars.csv", str);
        await BD.query(`copy cars("brand", "model", "start_year","finish_year","engine_volume","owner_id","highway_id") FROM '/home/igor/databases/course_work/data/cars.csv' WITH(FORMAT CSV,HEADER)`)
            .catch(err => { console.log(err) });
    },
    async GenerateHighways(amount) {
        const url = 'https://autostrada.info/ua/highways';
        let str = "code,name,characteristic,rating"
        let res;
        await axios.get(url)
            .then(responce => { res = responce.data })
            .catch(error => { console.log(error) })
        res = res.split("highwayLabel");
        res.splice(0, 1);
        for (let i = 1; i < res.length; i++) {
            res.splice(i, 1);
        }
        let arr = [];
        for (let i = 0; i < res.length; i++) {
            let buf = res[i].slice(res[i].indexOf("highway/"), res[i].indexOf("</a>")).split("highway/")[1];
            let buf1 = res[i].slice(res[i].indexOf("Доро"), res[i].indexOf('">\n\t\t\t'));
            let buf2 = res[i].slice(res[i].indexOf("rateNumber"), res[i].indexOf('</span>\t')).split('">')[1].split(",").join(".");
            arr.push(`${buf.split('">')[0]},${buf.split('">')[1]},"${buf1}",${buf2}`);
        }
        for (let i = 0; i < amount; i++) {
            str += `\n${arr[Math.floor(Math.random() * arr.length)]}`;
        }
        fs.writeFileSync("./data/highways.csv", str);
        await BD.query(`copy highways("code", "name", "characteristic","rating") FROM '/home/igor/databases/course_work/data/highways.csv' WITH(FORMAT CSV,HEADER)`)
            .catch(err => { console.log(err) });
    },
    async GenerateNumbers(amount) {
        let ids = await this.getIds("cars");
        var possible1 = "ABCEHIKMOPTX";
        var possible2 = "0123456789";
        let str = "data,release_date,car_id";
        for (let i = 0; i < amount; i++) {
            var text = "";
            text += possible1.charAt(Math.floor(Math.random() * possible1.length));
            text += possible1.charAt(Math.floor(Math.random() * possible1.length));
            text += " ";
            text += possible2.charAt(Math.floor(Math.random() * possible2.length));
            text += possible2.charAt(Math.floor(Math.random() * possible2.length));
            text += possible2.charAt(Math.floor(Math.random() * possible2.length));
            text += possible2.charAt(Math.floor(Math.random() * possible2.length));
            text += " ";
            text += possible1.charAt(Math.floor(Math.random() * possible1.length));
            text += possible1.charAt(Math.floor(Math.random() * possible1.length));

            str += `\n${text},${moment(this.generateRandomDate(new Date(2004, 0, 1), new Date(2013, 0, 1))).format('YYYY-MM-DD')},${ids[Math.floor(Math.random() * ids.length)]}`

        }
        fs.writeFileSync("./data/numbers.csv", str);
        await BD.query(`copy numbers("data", "release_date", "car_id") FROM '/home/igor/databases/course_work/data/numbers.csv' WITH(FORMAT CSV,HEADER)`)
            .catch(err => { console.log(err) });


    },
    getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    },
    generateRandomDate(start, end) {
        return new Date(new Date(start).getTime() + Math.random() * (new Date(end).getTime() - new Date(start).getTime()));
    }

};

module.exports = Model;


