const View = require('./View');
const readlineSync = require('readline-sync');
const Model = require('./Model');
const fs = require("fs");
const open = require("open")
const port = 3000;

async function CUI() {
    while (true) {
        View.display_commands();
        let inputs = readlineSync.question('Enter your command: ');
        switch (inputs) {
            case "":
                process.exit();
            case "1":
                console.log(`Choose table:
1.owners
2.numbers
3.cars
4.highways`);
                inputs = readlineSync.question('Enter your command: ');
                let amount = readlineSync.question('Enter amount of generated data: ');
                switch (inputs) {
                    case "1":
                        await Model.GenerateOwners(Number(amount));
                        break;
                    case "2":
                        await Model.GenerateNumbers(Number(amount));
                        break;
                    case "3":
                        await Model.GenerateCars(Number(amount));
                        break;
                    case "4":
                        await Model.GenerateHighways(Number(amount));
                        break;
                }
                break;
            case "2":
                console.log(`Choose table:
1.owners
2.numbers
3.cars
4.highways`);
                inputs = readlineSync.question('Enter your command: ');
                let fields, conditions, date_left, date_right, name, result;
                switch (inputs) {
                    case "1":
                        console.log(`Choose fields:
1.name
2.surname
3.pathronymic
4.date of birth`);
                        fields = readlineSync.question('Enter your command: ');
                        conditions = [];
                        if (fields.includes("1")) {
                            name = readlineSync.question('Input start of the owner`s name: ');
                            conditions.push(`(name like '${name}%')`);
                        }
                        if (fields.includes("2")) {
                            let surname = readlineSync.question('Input start of the owner`s surname: ');
                            conditions.push(`(surname like '${surname}%')`);
                        }
                        if (fields.includes("3")) {
                            let pathronymic = readlineSync.question('Input start of the owner`s pathronymic: ');
                            conditions.push(`(pathronymic like '${pathronymic}%')`);
                        }
                        if (fields.includes("4")) {
                            date_left = readlineSync.question('Input start of the owner`s birth: ');
                            date_left = View.date_validation(date_left);
                            date_right = readlineSync.question('Input end of the owner`s birth: ');
                            date_right = View.date_validation(date_right);
                            conditions.push(`(birth_date between '${date_left}' and '${date_right}')`);
                        }
                        fields = conditions.join(" and ");
                        result = await Model.DoQuery(`select name,pathronymic,surname,birth_date from owners where ${fields}`);
                        fs.writeFileSync('./public/info.json', JSON.stringify(result));
                        await open(`http://localhost:${port}/`);
                        break;
                    case "2":
                        console.log(`Choose fields:
1.data
2.release date`);
                        fields = readlineSync.question('Enter your command: ');
                        conditions = [];
                        if (fields.includes("1")) {
                            name = readlineSync.question('Input start of the number data: ');
                            conditions.push(`(data like '${name}%')`);
                        }
                        if (fields.includes("2")) {
                            date_left = readlineSync.question('Input start of the release date: ');
                            date_left = View.date_validation(date_left);
                            date_right = readlineSync.question('Input end of the release date: ');
                            date_right = View.date_validation(date_right);
                            conditions.push(`(release_date between '${date_left}' and '${date_right}')`);
                        }
                        fields = conditions.join(" and ");
                        result = await Model.DoQuery(`select data,release_date from numbers where ${fields}`);
                        fs.writeFileSync('./public/info.json', JSON.stringify(result));
                        await open(`http://localhost:${port}/`);
                        break;
                }
                break;
            case "3":
                console.log(`Choose query:
1.Колличество машин на трассах
2.numbers
3.cars
4.highways`);
                inputs = readlineSync.question('Enter your command: ');
                switch (inputs) {
                    case "1":
                        let result = await Model.DoQuery(`select name,count(*) as count from highways,cars where highway_id=highways.id
                        group by name
                        order by count(*) desc`)
                        fs.writeFileSync('./public/info.json', JSON.stringify(result));
                        let str = "Колличество машин на трассах"
                        result.forEach(element => {
                            str += `\n${element.name} ${element.count}`
                        });
                        fs.writeFileSync('./public/chart.txt', str);
                        await open(`http://localhost:${port}/`);
                        await open(`http://localhost:${port}/chart`);
                        break
                }
                break;
            case "4":
                console.log(`Choose query:
1.Список хазяев с отчеством Захарович и датой рождения между 1989.11.01 и 1990.01.01
2.Список машин с обьемом двигателя 4 л.с
3.cars
4.highways`);
                inputs = readlineSync.question('Enter your command: ');
                let query, date1, date2, str;
                switch (inputs) {
                    case "1":
                        query = "select * from owners where pathronymic='Лукич' and birth_date between '1989.11.01' and '1990.01.01'";
                        await Model.DoQuery("drop index in_2");
                        await Model.DoQuery("drop index in_3");
                        date1 = Date.now();
                        await Model.DoQuery(query);
                        date1 = Date.now() - date1;
                        await Model.DoQuery("create index in_2 on owners using btree(birth_date)");
                        await Model.DoQuery("create index in_3 on owners using btree(pathronymic)");
                        date2 = Date.now();
                        await Model.DoQuery(query);
                        date2 = Date.now() - date2;
                        str = "Время выполнения запроса с индексами и без"
                        str += `\nБез индекса ${date1}`;
                        str += `\nC индексом ${date2}`;
                        str += '\n 0';
                        fs.writeFileSync('./public/chart.txt', str);
                        await open(`http://localhost:${port}/chart`);
                        break;
                    case "2":
                        query = "select * from cars where engine_volume=4 ";
                        await Model.DoQuery("drop index in_1");
                        date1 = Date.now();
                        let result = await Model.DoQuery(query);
                        date1 = Date.now() - date1;
                        await Model.DoQuery("create index in_1 on cars using btree(engine_volume)");
                        date2 = Date.now();
                        await Model.DoQuery(query);
                        date2 = Date.now() - date2;
                        str = "Время выполнения запроса с индексами и без"
                        str += `\nБез индекса ${date1}`;
                        str += `\nC индексом ${date2}`;
                        str += '\n 0';
                        fs.writeFileSync('./public/chart.txt', str);
                        fs.writeFileSync('./public/info.json', JSON.stringify(result));
                        await open(`http://localhost:${port}/`);
                        await open(`http://localhost:${port}/chart`);
                        break;
                }
                break;
            case "5":
                console.log("main server shutdown...");
                Model.make_replication();
                console.log("slave is now the main server!");
                break;
            case "6":
                console.log(`Choose variant:
1.make database backup
2.recovery database from backup
3.make table backup
4.recovery table from backup`);
                inputs = readlineSync.question('Enter your command: ');
                switch (inputs) {
                    case "1":
                        inputs = readlineSync.question('Enter name of backup: ');
                        Model.make_db_backup(inputs);
                        break;
                    case "2":
                        Model.recovery_bd();
                        break;
                    case "3":
                        inputs = readlineSync.question('Enter name of table: ');
                        Model.make_table_backup(inputs);
                        break;
                    case "4":
                        await Model.recovery_table();
                        break;
                }
                break;
            case "7":
                Model.restore_master();
                break;
            default:
                View.Error_incorrect()
                break;

        }
    }
}

CUI();