const express = require('express');
const bodyParser = require('body-parser');
const mustache = require('mustache-express')
const path = require('path');
const morgan = require('morgan');
const busboyBodyParser = require('busboy-body-parser');


const app = express();

const viewsDir = path.join(__dirname, 'views');
const partialsDir= path.join(viewsDir,'partials');
app.engine('mst', mustache(partialsDir));
app.set('views', viewsDir);
app.set('view engine', 'mst');
app.use(morgan('dev'));
app.use(busboyBodyParser({ 
    limit: '10mb',
    multi: true,
}));
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function(req, res) {
    res.render('index');
});

app.get('/chart', function(req, res) {
    res.render('chart');
});

const port=3000;
app.listen(port, function () { console.log('Server is ready'); });
module.exports = port;