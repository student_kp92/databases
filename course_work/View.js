const readlineSync = require('readline-sync');
const moment = require('moment');

const View = {

    integer_validation(inputs) {
        let amount = Number(inputs);
        while (!Number.isInteger(amount) || amount <= 0) {
            console.log("This field must be an integer and >0");
            amount = readlineSync.question('Try again: ');
            amount = Number(amount);
        }
        return amount
    },
    date_validation(inputs) {
        while (!moment(inputs, moment.ISO_8601).isValid()) {
            console.log("Date must be ISO 8601 format. Try again.");
            inputs = readlineSync.question('Enter date: ');
        }
        return inputs
    },
    display_commands() {
        console.log(`1-generate data
2-filtration    
3-statistical analysis & graphics
4-indexes
5-replication
6-backups
Enter-exit program`);
    },
    
    outputTime(time) {
        console.log(`Query has been completed in ${time} ms.`)
    },
    table(object) {
        console.table(object);
    },
    Error_message(err) {
        console.log(`${err.name}: ${err.message}
Try again!`)
    },
    Error_empty() {
        console.log("This field can`t be empty! Try again!");
    },
    Sucess_message(id, Entity) {
        Entity = Entity.substring(0, Entity.length - 1);
        console.log(`New ${Entity} has been added with id ${id}!`);
    },
    Error_incorrect() {
        console.log("Incorrect input.Try again");
    },
    Input_table() {
        inputs = readlineSync.question('Enter table name: ');
        return inputs;
    }

}

module.exports = View;