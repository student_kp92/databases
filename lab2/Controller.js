const Model = require('../lab2/Model');
const View = require('../lab2/View');
const readlineSync = require('readline-sync');
const moment = require('moment');
const { Update, Join } = require('../lab2/Model');

const Controller = {

    async Show(Entity) {
        try {
            if (Entity === undefined) {
                Entity = View.Input_table();
            }
            View.table(await Model.GetEntity(Entity));
            return Entity;
        }
        catch (err) {
            console.log(err);
            await this.Show();
        }
    },

    async Insert(Entity, values) {
        try {
            await Model.Insert(Entity, values);
            const id = await Model.getLastId(Entity)
            View.Sucess_message(id, Entity);
            return id;
        }
        catch (err) {
            View.Error_message(err);
        }
    },
    async Insert_params(Entity, values) {
        try {
            const res = View.display_tables(Entity, values);
            await Model.Insert(res[0], res[1]);
            const id = await Model.getLastId(Entity)
            View.Sucess_message(id);
            return id;
        }
        catch (err) {
            View.Error_message(err);
            await this.Insert_params(Entity, values);
        }
    },

    async Delete(Entity, field, value) {
        try {
            await Model.Delete(Entity, field, value);
            console.log("Deleted successfully!");
        }
        catch (err) {
            View.Error_message(err);
        }
    },

    async Update(Entity, values, id) {
        try {
            await Model.Update(Entity, values, id)
            console.log("Updated!")
        }
        catch (err) {
            View.Error_message(err);
        }
    },

    async Insert_rand(Entity, amount, mode) {
        try {
            let time = new Date().getTime();
            await Model.InsertRandom(Entity, amount, mode).then(res => {
                time = new Date().getTime() - time;
            });
            View.outputTime(time);
        }
        catch (err) {
            View.Error_message(err);
        }
    },

    async Join(Entities, fields, condition) {
        try {
            let time = new Date().getTime();
            View.table(await Model.Join(Entities, fields, condition));
            time = new Date().getTime() - time;
            View.outputTime(time);
        }
        catch (err) {
            View.Error_message(err);
        }
    },
    async CUI() {
        while (true) {
            View.display_commands();
            let inputs = readlineSync.question('Enter your command: ');
            switch (inputs) {
                case "":
                    process.exit();
                case "1":
                    while (true) {
                        inputs = readlineSync.question('Enter table name: ');
                        try {
                            View.table(await Model.GetEntity(inputs));
                            break;
                        }
                        catch (err) { View.Error_message(err) }
                    }
                    break;
                case "2":
                    View.display_tables();
                    inputs = readlineSync.question('Enter your command: ');
                    switch (inputs) {
                        case "1":
                            let user_id = 0;
                            let message_id = 0;
                            while (true) {
                                const username = readlineSync.question('Enter username: ');
                                if (username.length === 0) {
                                    View.Error_empty();
                                    continue;
                                }
                                const password = readlineSync.question('Enter password: ');
                                if (password.length === 0) {
                                    View.Error_empty();
                                    continue;
                                }
                                try {
                                    await Model.Insert('users', [username, password]);
                                    user_id = await Model.getLastId("users")
                                    View.Sucess_message(user_id);
                                    break;
                                }
                                catch (err) { View.Error_message(err) }
                            }
                            inputs = readlineSync.question('Would you like to send a message on behalf of this user?[y/n] : ');
                            while (inputs != "y" && inputs != "n") {
                                console.log("This field can be only y or n");
                                inputs = readlineSync.question('Would you like to send a message on behalf of this user?[y/n]: ');
                            }
                            if (inputs === "y") {
                                inputs = readlineSync.question(`Whether you want to create a new message or select an existing one?
1-new
2-existing`);
                                while (inputs != "1" && inputs != "2") {
                                    console.log("This field can be only 1 or 2");
                                    inputs = readlineSync.question(`Whether you want to create a new message or select an existing one?
1-new
2-existing`);
                                }
                                if (inputs === "1") {
                                    let values = [];
                                    inputs = readlineSync.question('Enter date of message: ');
                                    while (!moment(inputs, moment.ISO_8601).isValid()) {
                                        console.log("Date must be ISO 8601 format. Try again.");
                                        inputs = readlineSync.question('Enter date: ');
                                    }
                                    values.push(inputs);
                                    inputs = readlineSync.question('Enter message status[1-sent/2-not sent]: ');
                                    while (inputs.length === 0 || (inputs != "1" && inputs != "2")) {
                                        console.log("This field can be only 1 or 2");
                                        inputs = readlineSync.question('Enter message status[1-sent/2-not sent]: ');
                                    }
                                    if (inputs === 1) values.push(true)
                                    else values.push(false)
                                    inputs = readlineSync.question('Enter content of message: ');
                                    while (inputs.length === 0) {
                                        console.log("This field can`t be empty. Try again.");
                                        inputs = readlineSync.question('Enter content of message: ');
                                    }
                                    values.push(inputs);
                                    try {
                                        await Model.Insert('messages', values);
                                        message_id = await Model.getLastId("messages");
                                        View.Sucess_message(message_id);
                                    }
                                    catch (err) { View.Error_message(err) }
                                    try {
                                        await Model.Insert("users_messages", [user_id, message_id]);
                                        console.log(`Message with id ${message_id} has been added to user with id ${user_id}`);
                                    }
                                    catch (err) { View.Error_message(err) }
                                    break;
                                }
                                else {
                                    while (true) {
                                        inputs = readlineSync.question('Enter table name: ');
                                        try {
                                            View.table(await Model.GetEntity(inputs));
                                            break;
                                        }
                                        catch (err) { View.Error_message(err) }
                                    }



                                }
                            }
                            else break;
                    }
                    break;
            }
            break;
        }
    }
}

module.exports = Controller;

//Model.Insert('users', ["hui", "1"]); 