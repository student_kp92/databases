const { Client } = require('pg');

const Email = new Client({
    host: "localhost",
    port: 5432,
    user: "postgres",
    database: "Email",
    password: "1111"
})

Email.connect();

const Model = {

    async getTypes(Entity) {
        let types = [];
        await Email.query(`SELECT * FROM ${Entity}`).catch(err => { throw new Error(err) }).then(res => {
            res.fields.forEach((element, id) => {
                if (id != 0) {
                    types.push(element.dataTypeID);
                }
            })
        });
        return types;
    },

    async getFields(Entity) {
        let fields = [];
        await Email.query(`SELECT * FROM ${Entity}`).catch(err => { throw new Error(err) }).then(res => {
            res.fields.forEach((element, id) => {
                if (element.name != "id") {
                    fields.push(element.name);
                }
            })
        });
        return fields;
    },

    async getLastId(entity) {
        let id;
        await Email.query(`SELECT "id" FROM ${entity}`).then(data => {
            id = data.rows[data.rows.length - 1].id;
        })
        return id;
    },

    async Insert(Entity, values) {
        let fields = await this.getFields(Entity);
        values.forEach((element, id, values) => {
            if (typeof (element) === "string") {
                values[id] = `'${element}'`;
            }
        });
        const query = `INSERT INTO ${Entity} (${fields}) values(${values})`;
        await Email.query(query).catch(err => { throw new Error(err) });
    },

    async InsertRandom(Entity, amount, spec_mode) {
        let curr_id;
        await this.getLastId(Entity).then(data => { curr_id = data });
        let fields = [];
        await this.getFields(Entity).then(res => { fields = res });
        let types = [];
        await this.getTypes(Entity).then(res => { types = res });
        let values = [];
        types.forEach((element, id) => {
            if (element === 25) {
                values[id] = "chr(trunc(65+random()*25)::int) || chr(trunc(65+random()*25)::int ) || chr(trunc(65+random()*25)::int ) || chr(trunc(65+random()*25)::int) || chr(trunc(65+random()*25)::int) || chr(trunc(65+random()*25)::int)";
            }
            if (element === 23) {
                values[id] = "trunc(random()*1000)::int";
            }
            if (element === 1082) {
                values[id] = "timestamp '2014-01-10 20:00:00' +random() * (timestamp '2020-01-20 20:00:00' -timestamp '2014-01-10 10:00:00')";
            }
            if (element === 16) {
                values[id] = "(round(random())::int)::boolean";
            }
        });
        if (spec_mode != undefined) {
            const last = await this.getLastId("users");
            values[3] = `trunc(random()*${last})::int`;
        }
        await Email.query(`INSERT INTO ${Entity} (${fields}) SELECT ${values} FROM generate_series(${curr_id + 1},${amount}+${curr_id})`)
    },

    async Delete(Entity, field, value) {

        if (typeof (value) === "string") {
            value = `'${value}'`;
        }
        const query = `DELETE FROM ${Entity} WHERE ${field}=${value}`;
        await Email.query(query).catch(err => { throw new Error(err) });
    },


    async Update(Entity, values, id) {
        let fields = [];
        await this.getFields(Entity).then(res => { fields = res });
        values.forEach((element, id, values) => {
            if (typeof (element) === "string") {
                values[id] = `'${element}'`;
            }
        });
        const query = `UPDATE ${Entity} SET (${fields}) = (${values}) WHERE "id" =${id}`;
        await Email.query(query).catch(err => { throw new Error(err) });
    },

    async GetEntity(Entity) {
        let result;
        const query = `SELECT * FROM ${Entity}`;
        await Email.query(query).catch(err => { throw new Error(err) }).then(res => { result = res.rows });
        return result;
    },

    async GetItemID(Entity, field, value) {
        let result = [];
        const query = `SELECT * FROM ${Entity} WHERE ${field}=${value}`;
        await Email.query(query).catch(err => { throw new Error(err) }).then(res => {
            res.rows.forEach(element => {
                result.push(element.id)
            })
        });
        return result;
    },
    async Join(Entities, fields, condition) {
        let result;
        const query = `SELECT ${fields} FROM ${Entities} WHERE ${condition}`;
        await Email.query(query).catch(err => { throw new Error(err) }).then(res => { result = res.rows });
        return result;

    },

};

module.exports = Model;