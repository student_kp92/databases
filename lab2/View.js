const readlineSync = require('readline-sync');
const moment = require('moment');
//const Controller = require('./Controller');

const View = {

    integer_validation(inputs) {
        let amount = Number(inputs);
        while (!Number.isInteger(amount) || amount <= 0) {
            console.log("This field must be an integer and >0");
            amount = readlineSync.question('Try again: ');
            amount = Number(amount);
        }
        return amount
    },
    date_validation(inputs) {
        while (!moment(inputs, moment.ISO_8601).isValid()) {
            console.log("Date must be ISO 8601 format. Try again.");
            inputs = readlineSync.question('Enter date: ');
        }
        return inputs
    },
    display_commands() {
        console.log(`1-look
2-insert
3-delete
4-update
5-fill with random data
6-special commands
Enter-exit program`);
    },
    async update_table() {
        console.log("Choose table:");
        console.log(`1-users
2-folders
3-messages`);
        while (1) {
            inputs = readlineSync.question('Enter table name: ');
            if (inputs === "1") {

                const username = readlineSync.question('Enter new username: ');
                const password = readlineSync.question('Enter new password: ');
                return ["users", [username, password]];
            }
            else if (inputs === "2") {
                const name = readlineSync.question('Enter name: ');
                let amount = readlineSync.question('Enter amount of messages: ');
                amount = Number(amount);
                while (!Number.isInteger(amount) || amount <= 0) {
                    console.log("This field must be an integer and >0. Try again.");
                    amount = readlineSync.question('Enter new amount of messages: ');
                    amount = Number(amount);
                }
                return ["folders", [name, amount]];
            }
            else if (inputs === "3") {
                let values = [];
                inputs = readlineSync.question('Enter date of message: ');
                while (!moment(inputs, moment.ISO_8601).isValid()) {
                    console.log("Date must be ISO 8601 format. Try again.");
                    inputs = readlineSync.question('Enter date: ');
                }
                values.push(inputs);
                inputs = readlineSync.question('Enter message status[1-sent/2-not sent]: ');
                while (inputs.length === 0 || (inputs != "1" && inputs != "2")) {
                    console.log("This field can be only 1 or 2");
                    inputs = readlineSync.question('Enter message status[1-sent/2-not sent]: ');
                }
                if (inputs === 1) values.push(true)
                else values.push(false)
                inputs = readlineSync.question('Enter content of message: ');
                while (inputs.length === 0) {
                    console.log("This field can`t be empty. Try again.");
                    inputs = readlineSync.question('Enter content of message: ');
                }
                values.push(inputs);
                inputs = readlineSync.question('Message should have user.Would you like to 1.create a new user or 2.choose an existing one?[1/2] : ');
                while (inputs != "1" && inputs != "2") {
                    console.log("This field can be only y or n");
                    inputs = readlineSync.question('Message should have user.Would you like to 1.create a new user or 2.choose an existing one?[1/2] : ');
                }
                if (inputs === "1") {
                    return ["messages", values, 1];
                }
                else {
                    return ["messages", values];
                }
            }
            else {
                continue;
            }
        }
    },
    async display_tables(name, value) {
        if (name === undefined) {
            console.log("Choose table:");
            console.log(`1-users
2-folders
3-messages`);
        }
        while (1) {
            if (name === undefined) {
                inputs = readlineSync.question('Enter table name: ');
            }
            else {
                inputs = name;
            }
            if (inputs === "1") {
                const username = readlineSync.question('Enter username: ');
                const password = readlineSync.question('Enter password: ');
                return ["users", [username, password]];
            }
            else if (inputs === "2") {
                const name = readlineSync.question('Enter name: ');
                let amount = readlineSync.question('Enter amount of messages: ');
                amount = Number(amount);
                while (!Number.isInteger(amount) || amount <= 0) {
                    console.log("This field must be an integer and >0. Try again.");
                    amount = readlineSync.question('Enter new amount of messages: ');
                    amount = Number(amount);
                }
                return ["folders", [name, amount]];
            }
            else if (inputs === "3") {
                let values = [];
                inputs = readlineSync.question('Enter date of message: ');
                while (!moment(inputs, moment.ISO_8601).isValid()) {
                    console.log("Date must be ISO 8601 format. Try again.");
                    inputs = readlineSync.question('Enter date: ');
                }
                values.push(inputs);
                inputs = readlineSync.question('Enter message status[1-sent/2-not sent]: ');
                while (inputs.length === 0 || (inputs != "1" && inputs != "2")) {
                    console.log("This field can be only 1 or 2");
                    inputs = readlineSync.question('Enter message status[1-sent/2-not sent]: ');
                }
                if (inputs === 1) values.push(true)
                else values.push(false)
                inputs = readlineSync.question('Enter content of message: ');
                while (inputs.length === 0) {
                    console.log("This field can`t be empty. Try again.");
                    inputs = readlineSync.question('Enter content of message: ');
                }
                values.push(inputs);
                inputs = readlineSync.question('Message should have user.Would you like to 1.create a new user or 2.choose an existing one?[1/2] : ');
                while (inputs != "1" && inputs != "2") {
                    console.log("This field can be only y or n");
                    inputs = readlineSync.question('Message should have user.Would you like to 1.create a new user or 2.choose an existing one?[1/2] : ');
                }
                if (inputs === "1") {
                    return ["messages", values, 1];
                }
                else {
                    return ["messages", values];
                }
            }
            else {
                continue;
            }
        }
    },
    outputTime(time) {
        console.log(`Query has been completed in ${time} ms.`)
    },
    table(object) {
        console.table(object);
    },
    Error_message(err) {
        console.log(`${err.name}: ${err.message}
Try again!`)
    },
    Error_empty() {
        console.log("This field can`t be empty! Try again!");
    },
    Sucess_message(id, Entity) {
        Entity = Entity.substring(0, Entity.length - 1);
        console.log(`New ${Entity} has been added with id ${id}!`);
    },
    Error_incorrect() {
        console.log("Incorrect input.Try again");
    },
    Input_table() {
        inputs = readlineSync.question('Enter table name: ');
        return inputs;
    }

}

module.exports = View;