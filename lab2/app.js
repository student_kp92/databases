const View = require('./View');
const readlineSync = require('readline-sync');
const moment = require('moment');
const Controller = require('./Controller');
const Model = require('../lab2/Model');

async function CUI() {
    while (true) {
        View.display_commands();
        let inputs = readlineSync.question('Enter your command: ');
        switch (inputs) {
            case "":
                process.exit();
            case "1":
                await Controller.Show();
                break;
            case "2":
                let res = await View.display_tables();
                if (res[0] === "users") {
                    await Controller.Insert("users", res[1]);
                }
                if (res[0] === "folders") {
                    await Controller.Insert("folders", res[1]);
                }
                if (res[0] === "messages") {
                    let mes_id;
                    if (res[2] === 1) {
                        const result = await View.display_tables("1");
                        const user_id = await Controller.Insert("users", result[1]);
                        res[1].push(user_id);
                        mes_id = await Controller.Insert("messages", res[1]);
                    }
                    else {
                        await Controller.Show("users");
                        inputs = readlineSync.question('Choose user id: ');
                        let amount = Number(inputs);
                        while (!Number.isInteger(amount) || amount <= 0) {
                            console.log("This field must be an integer and >0. Try again.");
                            amount = readlineSync.question('Choose user id: ');
                            amount = Number(amount);
                        }
                        res[1].push(amount);
                        mes_id = await Controller.Insert("messages", res[1]);
                    }
                    await Controller.Show("folders");
                    inputs = readlineSync.question('Message should have folder.Chose folder id: ');
                    let amount = Number(inputs);
                    while (!Number.isInteger(amount) || amount <= 0) {
                        console.log("This field must be an integer and >0. Try again.");
                        amount = readlineSync.question('Choose user id: ');
                        amount = Number(amount);
                    }
                    try {
                        await Model.Insert("folders_messages", [amount, mes_id])
                    }
                    catch (err) {
                        View.Error_message(err);
                    }
                }
                break;
            case "3":
                console.log("Choose table:");
                console.log(`1-users
2-folders
3-messages`);
                while (1) {
                    inputs = readlineSync.question('Enter table name: ');
                    if (inputs === "1") {
                        View.table(await Model.GetEntity("users"));
                        inputs = readlineSync.question('Choose user by id: ');
                        let amount = Number(inputs);
                        while (!Number.isInteger(amount) || amount <= 0) {
                            console.log("This field must be an integer and >0. Try again.");
                            amount = readlineSync.question('Choose user id: ');
                            amount = Number(amount);
                        }
                        const ids = await Model.GetItemID("messages", "user_id", amount);
                        console.log(ids)
                        for (let i = 0; i < ids.length; i++) {
                            await Controller.Delete("folders_messages", "message_id", ids[i]);
                        }
                        await Controller.Delete("messages", "user_id", amount);
                        await Controller.Delete("users", "id", amount);
                        break;
                    }
                    if (inputs === "2") {
                        View.table(await Model.GetEntity("folders"));
                        inputs = readlineSync.question('Choose folder by id: ');
                        let amount = Number(inputs);
                        while (!Number.isInteger(amount) || amount <= 0) {
                            console.log("This field must be an integer and >0. Try again.");
                            amount = readlineSync.question('Choose user id: ');
                            amount = Number(amount);
                        }
                        await Controller.Delete("folders_messages", "folder_id", amount);
                        await Controller.Delete("folders", "id", amount);
                        break;
                    }
                    if (inputs === "3") {
                        View.table(await Model.GetEntity("messages"));
                        inputs = readlineSync.question('Choose message by id: ');
                        let amount = Number(inputs);
                        while (!Number.isInteger(amount) || amount <= 0) {
                            console.log("This field must be an integer and >0. Try again.");
                            amount = readlineSync.question('Choose user id: ');
                            amount = Number(amount);
                        }
                        await Controller.Delete("folders_messages", "message_id", amount);
                        await Controller.Delete("messages", "id", amount);
                        break;
                    }
                }
                break;
            case "4":
                const Entity = await Controller.Show();
                if (Entity === "users") {
                    inputs = readlineSync.question('Choose user by id: ');
                    const id = View.integer_validation(inputs);
                    const username = readlineSync.question('Enter new username: ');
                    const password = readlineSync.question('Enter new password: ');
                    await Controller.Update("users", [username, password], id);
                    break;
                }
                if (Entity === "folders") {
                    inputs = readlineSync.question('Choose folder by id: ');
                    const id = View.integer_validation(inputs);
                    const name = readlineSync.question('Enter name: ');
                    let amount = readlineSync.question('Enter amount of messages: ');
                    amount = View.integer_validation(amount);
                    await Controller.Update("folders", [name, amount], id);
                    break;
                }
                if (Entity === "messages") {
                    inputs = readlineSync.question('Choose message by id: ');
                    const id = View.integer_validation(inputs);
                    let date = readlineSync.question('Enter date of message: ');
                    date = View.date_validation(date);
                    let status = readlineSync.question('Enter message status[1-sent/2-not sent]: ');
                    while (status.length === 0 || (status != "1" && status != "2")) {
                        console.log("This field can be only 1 or 2");
                        status = readlineSync.question('Enter message status[1-sent/2-not sent]: ');
                    }
                    if (status === 1) status = true;
                    else status = false;
                    let content = readlineSync.question('Enter content of message: ');
                    await Controller.Update("messages", [date, status, content], id)
                }
                break;
            case "5":
                const en = readlineSync.question('Choose table: ');
                let am = readlineSync.question('Choose amount of items: ');
                am = View.integer_validation(am);
                if (en === "messages") {
                    await Controller.Insert_rand(en, am, 1);
                }
                else {
                    await Controller.Insert_rand(en, am);
                }
                break;
            case "6":
                inputs = readlineSync.question(`Choose variant:
1.Filter users and messages by login,date and status
2.Filter folders and messages by name,amount and content
3.Filter users,folders and messages by password,date,status and amount
Enter your command: `);
                const variant = View.integer_validation(inputs);
                if (variant === 1) {
                    let name_start = readlineSync.question('Input start of the user`s login: ');
                    let date_left = readlineSync.question('Input start of the message`s date: ');
                    date_left = View.date_validation(date_left);
                    let date_right = readlineSync.question('Input end of the message`s date: ');
                    date_right = View.date_validation(date_right);
                    let status = readlineSync.question('Enter message`s status[1-sent/2-not sent]: ');
                    while (status.length === 0 || (status != "1" && status != "2")) {
                        console.log("This field can be only 1 or 2");
                        status = readlineSync.question('Enter message status[1-sent/2-not sent]: ');
                    }
                    if (status === "1") status = true;
                    else status = false;
                    await Controller.Join("users,messages", "users.id,login,date,status", `(user_id=users.id) and (login like '${name_start}%') and (date between '${date_left}' and '${date_right}') and status=${status}`);
                    break;
                }
                if (variant === 2) {
                    let name_start = readlineSync.question('Input start of the folder`s name: ');
                    let amount_left = readlineSync.question('Input lower limit of folder`s amount of messages: ');
                    amount_left = View.integer_validation(amount_left);
                    let amount_right = readlineSync.question('Input upper limit of folder`s amount of messages: ');
                    amount_right = View.integer_validation(amount_right);
                    let content_start = readlineSync.question('Input start of the message`s content: ');
                    await Controller.Join("folders,messages,folders_messages", "messages.id,content,name,amount", `(folder_id=message_id) and (content like '${content_start}%') and (amount between '${amount_left}' and '${amount_right}') and (name like '${name_start}%')`);
                    break;
                }
                if (variant === 3) {
                    let pass_start = readlineSync.question('Input start of the user`s password: ');
                    let date_left = readlineSync.question('Input start of the message`s date: ');
                    date_left = View.date_validation(date_left);
                    let date_right = readlineSync.question('Input end of the message`s date: ');
                    date_right = View.date_validation(date_right);
                    let status = readlineSync.question('Enter message`s status[1-sent/2-not sent]: ');
                    while (status.length === 0 || (status != "1" && status != "2")) {
                        console.log("This field can be only 1 or 2");
                        status = readlineSync.question('Enter message status[1-sent/2-not sent]: ');
                    }
                    if (status === "1") status = true;
                    else status = false;
                    let amount_left = readlineSync.question('Input lower limit of folder`s amount of messages: ');
                    amount_left = View.integer_validation(amount_left);
                    let amount_right = readlineSync.question('Input upper limit of folder`s amount of messages: ');
                    amount_right = View.integer_validation(amount_right);
                    await Controller.Join("users,folders,messages,folders_messages", "users.id,password,date,status,amount", `(users.id=user_id) and (folder_id=message_id) and (password like '${pass_start}%') and (amount between '${amount_left}' and '${amount_right}') and (date between '${date_left}' and '${date_right}') and (status=${status})`);
                    break;
                }
            default:
                View.Error_incorrect()
                break;

        }
    }
}

CUI();